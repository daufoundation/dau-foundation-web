import { Button, CircularProgress, Collapse, Grid, IconButton, TextField } from "@material-ui/core";
import { Box } from "@mui/system";
import React, { useState } from "react";
import ConfigData from '../config.json'
import axios from "axios";
import { Alert } from "@mui/material";
import { Close } from "@mui/icons-material";
import { useHistory } from "react-router";


function CreateWallet(){
    const [address, setAddress] = useState()
    const [walletName, setWalletName] = useState("")
    const [loading, setLoading] = useState(false)
    const [getWalletSuccess, setGetwalletSuccess] = useState()
    const [getWalletError, setGetWalletError] = useState()
    const [password, setPassword] = useState("")
    const history = useHistory()
    const API = ConfigData.API
    
    function handleCreate(event){
        event.preventDefault()
        console.log(walletName)
        setLoading(true)
        console.log(address)
        axios.post(API+"/wallet/create",{wallet_name: walletName, private_key: password}, {
            withCredentials: false,
            headers: { 'Content-Type': 'application/json'
          }})
        .then((resp)=>{
            console.log(resp)
            setGetwalletSuccess(true)
            setLoading(false)
            // keep wallet 
            var wallets = JSON.parse(localStorage.getItem("wallets"))
            wallets.push({wallet_name:walletName, balance:0, wallet_address:resp.data.address})
            console.log(wallets)
            localStorage.setItem("wallets", JSON.stringify(wallets))
            
            history.push("/")
        }, (error)=>{
            console.log(error.response.data)
            setGetWalletError(true)
            setLoading(false)
        })
    }
    return (
        <Grid container direction="column" justifyContent="center" >
            <Grid item>
                <Box textAlign="center" spacing={5} m={5} >
                    <h2  alignContent="center"><img style={{width:"100px"}} src="../daulogo.svg"/></h2>
                </Box>
            </Grid>
            <Grid item>
                <Collapse in={getWalletSuccess}>
                    <Alert
                        action={
                        <IconButton
                            aria-label="close"
                            color="inherit"
                            size="small"
                            onClick={() => {
                            setGetwalletSuccess(false)
                            }}
                        >
                            <Close fontSize="inherit" />
                        </IconButton>
                        }
                    >
                        Order created successfully!
                    </Alert>
                </Collapse>
                <Collapse in={getWalletError}>
                    <Alert
                        severity="error"
                        action={
                        <IconButton
                            aria-label="close"
                            color="inherit"
                            size="small"
                            onClick={() => {
                            setGetWalletError(false)
                            }}
                        >
                            <Close fontSize="inherit" />
                        </IconButton>
                        }
                    >
                        Sorry, error getting wallet
                    </Alert>
                </Collapse>
                <form onSubmit={handleCreate}>
                    <Box m={5}>
                        <TextField
                        required
                        value={walletName}
                        onChange={(event)=>{setWalletName(event.target.value)}}
                        label="Wallet Name"/>
                    </Box>
                    <Box m={5}>
                        <TextField
                        required
                        value ={password}
                        onChange={(event)=>{setPassword(event.target.value)}}
                        label="Password"/>
                    </Box>
                    <Box m={5}>
                        <Button type="submit" variant="contained" color="primary">
                            {loading? "": "Create Wallet"}
                            {loading && <CircularProgress size={27} />}
                        </Button>
                    </Box>
                </form>
            </Grid>
        </Grid>
    )

}


export default CreateWallet