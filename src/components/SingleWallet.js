import { Button, Grid, IconButton, List, ListItem, ListItemIcon, ListItemText, Snackbar, TextField, Typography } from "@material-ui/core";
import { ListItemButton } from "@mui/material";
import { Box } from "@mui/system";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router"
import AccountBalanceWalletIcon from '@mui/icons-material/AccountBalanceWallet';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import {useHistory} from 'react-router-dom'
import { Close, CopyAll, Delete, QrCode, SendOutlined, ViewAgenda } from "@mui/icons-material";
import ConfigData from '../config.json'
import axios from "axios";



function SingleWallet(){
    const history = useHistory()
    const {address} = useParams()
    const [walletName, setWalletName] = useState("")
    const [walletBalance, setWalletBalance] = useState(0)
    const [blocks, setBlocks] = useState([])
    const API = ConfigData.API
    const [isCopied, setIsCopied] = useState(false)
    const wallets = JSON.parse(localStorage.getItem("wallets"))

    useEffect(()=>{
        axios.post(API+"/wallet/info",{Address: address}, {
            withCredentials: false,
            headers: { 'Content-Type': 'application/json'
          }})
        .then((resp)=>{
            console.log(resp)
            setWalletName(resp.data.data.wallet_name)
            setWalletBalance(resp.data.data.balance)
            axios.post(API+"/wallet/print",{Address: address}, {
                withCredentials: false,
                headers: { 'Content-Type': 'application/json'
              }})
            .then((resp)=>{
                console.log(resp)
                setBlocks(resp.data)
                console.log(blocks)
            }, (error)=>{
          
            })
        }, (error)=>{
      
        })

   
    }, [])


    const handleOpenCopiedToast = (Transition) => () => {
      
    };
    
    const handleCloseToast = () => {
        setIsCopied(false);
    };
    
    function CopyText(){
        navigator.clipboard.writeText(address);
        setIsCopied(true);
    }

   function deleteSpecific(item, index, arr){
       if (!item){
           return
       }
        console.log(item)

       if (item.wallet_address == address){
           arr.splice(index, 1)
       }
    }
    function deletWallet(){
       
        wallets.forEach(deleteSpecific)
        localStorage.setItem("wallets", JSON.stringify(wallets))
        history.push("/")
    }
    const action = (
        <React.Fragment>
          <IconButton
            size="small"
            aria-label="close"
            color="inherit"
            onClick={handleCloseToast}
          >
            <Close fontSize="small" />
          </IconButton>
        </React.Fragment>
      );
    return (
        <Grid container justifyContent="center" alignContent="center" alignItems="center" direction="column" >
            <Grid item>
                <Box textAlign="center" spacing={5} m={5} >
                    <h2  alignContent="center"><img style={{width:"100px"}} src="../daulogo.svg"/></h2>
                </Box>
            </Grid>
            <Grid item>
                <Typography variant="h4">{walletName}</Typography>
            </Grid>
            <Grid item>
                <Typography variant="h3">{parseInt(walletBalance).toLocaleString()}</Typography>
            </Grid>
            <Grid item>
                <Grid container>
                    <Grid item spacing={5}>
                        <Box m={5}>
                            <IconButton onClick={()=>{history.push("/send/"+address)}}>
                                <SendOutlined/>
                            </IconButton>
                        </Box>
                    </Grid>
                    <Grid item spacing={5}>
                        <Box m={5}>
                            <IconButton>
                                <QrCode/>
                            </IconButton>
                        </Box>
                    </Grid>
                    <Grid item spacing={5}>
                        <Box m={5}>
                            <IconButton onClick={CopyText}>
                                <CopyAll/>
                            </IconButton>
                            <Snackbar
                                open={isCopied}
                                onClose={handleCloseToast}
                                message="Wallet Address Copied!"
                                action={action}
                                autoHideDuration={4000}
                            />
                        </Box>
                    </Grid>
                    <Grid item spacing={5}>
                        <Box m={5}>
                            <IconButton onClick={deletWallet}>
                                <Delete/>
                            </IconButton>
                        </Box>
                    </Grid>
                </Grid>
            </Grid>
            <Grid item spacing={5}>
                <Box sx={{ width: '100%', bgcolor: 'background.paper' }}>
                    <nav aria-label="secondary mailbox folders">
                        <List>
                            <ListItem >
                                <ListItemText primary="Transactions" />
                            </ListItem>
                            {
                                blocks.map((block)=>(
                                    <ListItem >
                                        <ListItemButton>  
                                            <ListItemIcon>
                                                <ViewAgenda />
                                            </ListItemIcon>
                                            <ListItemText  primary={block.amount.toLocaleString()} secondary={(block.reciever==address? block.sender: block.reciever)}  />
                                        </ListItemButton>
                                    </ListItem>
                                ))
                            }
                        
                    
                        </List>
                    </nav>
                </Box>
            </Grid>
        </Grid>
    )

}


export default SingleWallet