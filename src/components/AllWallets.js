import { Button, Grid, List, ListItem, ListItemIcon, ListItemText, TextField, Typography } from "@material-ui/core";
import { ListItemButton } from "@mui/material";
import { Box } from "@mui/system";
import React, { useEffect } from "react";
import AccountBalanceWalletIcon from '@mui/icons-material/AccountBalanceWallet';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import {useHistory} from 'react-router-dom'
import ConfigData from '../config.json'
import axios from "axios";


function AllWallets(){
    const history = useHistory()
    var wallets = JSON.parse(localStorage.getItem("wallets"))
    const API = ConfigData.API

    if (!wallets){
        var newwallets = []
        localStorage.setItem("wallets", JSON.stringify(newwallets))
        wallets = JSON.parse(localStorage.getItem("wallets"))
    }

    function getWalletData(item, index, arr){
        if (!item){
            return
        }
        axios.post(API+"/wallet/info",{Address: item.wallet_address}, {
            withCredentials: false,
            headers: { 'Content-Type': 'application/json'
          }})
        .then((resp)=>{
            console.log(resp)
            arr[index]= resp.data.data
        }, (error)=>{
      
        })
    }
    useEffect(()=>{
        if (!wallets){
            localStorage.setItem("wallets", JSON.stringify(newwallets))
            wallets = JSON.parse(localStorage.getItem("wallets"))
        }
        wallets = JSON.parse(localStorage.getItem("wallets"))
        wallets.forEach(getWalletData)
    }, [])
  
    return (
        <Grid container justifyContent="center" alignContent="center" alignItems="center" direction="column" >
            <Box textAlign="center" spacing={5} m={5} >
                <h2  alignContent="center"><img style={{width:"100px"}} src="../daulogo.svg"/></h2>
            </Box>
            <Grid item>
                <Typography variant="h4">All Wallets</Typography>
            </Grid>
            <Grid item>
                <Grid container>
                    <Grid item spacing={5}>
                        <Box m={5}>
                        <Button onClick={()=>{history.push("/create_wallet")}} variant="contained" color="primary">Create Wallet</Button>
                        </Box>
                    </Grid>
                    <Grid item spacing={5}>
                        <Box m={5}>
                        <Button onClick={()=>{history.push("/add_wallet")}} variant="contained" color="primary">Add Wallet</Button>
                        </Box>
                    </Grid>
                    
                </Grid>
            </Grid>
            <Grid item spacing={5}>
                <Box sx={{ width: '100%', bgcolor: 'background.paper' }}>
                    <nav aria-label="secondary mailbox folders">
                        <List>
                            <ListItem >
                                <ListItemText primary="MY ASSETS " />
                            </ListItem>
                            {
                                wallets.map((wallet, i)=>(
                                    wallet?
                                    <ListItem key={i} >
                                        <ListItemButton onClick={()=>{history.push("/wallet/"+wallet.wallet_address)}}>  
                                            <ListItemIcon >
                                                <AccountBalanceWalletIcon />
                                            </ListItemIcon>
                                            <ListItemText primary={wallet.wallet_name} />
                                        </ListItemButton>
                                    </ListItem>
                                    :""
                                ))
                            }
                        </List>
                    </nav>
                </Box>
            </Grid>
        </Grid>
    )

}


export default AllWallets