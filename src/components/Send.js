import { Button, CircularProgress, Collapse, Grid, IconButton, TextField, Typography } from "@material-ui/core";
import { Close, QrCode } from "@mui/icons-material";
import { Alert } from "@mui/material";
import { Box } from "@mui/system";
import axios from "axios";
import React, { useState } from "react";
import { useHistory, useParams } from "react-router";
import COnfig from '../config.json'

function SendCoin(){

    const [recieverAddress, setRecieverAddress] = useState("")
    const [password, setPassword] = useState("")
    const [amount, setAmount] = useState(0)
    const API = COnfig.API
    const [loading, setLoading] = useState(false)
    const [sendSuccess, setSendSuccess] = useState(false)
    const [sendError, setSendError] = useState(false)
    const {address} = useParams()
    const history = useHistory()


    function handleSend(event){
        setLoading(true)
        event.preventDefault()
        axios.post(API+"/wallet/transfer", {sender_address:address, amount:amount, reciever_address:recieverAddress, sender_private_key: password},
        {
            withCredentials: false,
            headers: { 'Content-Type': 'application/json'}})
        .then((resp)=>{
            setSendSuccess(true)
            setLoading(false)
            history.goBack()
        }, (error)=>{
            console.log(error)
            setLoading(false)
            setSendError(true)
        })
    }
    return (
        <Grid container direction="column" justifyContent="center" >
            <Grid item>
                <Box textAlign="center" spacing={5} m={5} >
                    <h2  alignContent="center"><img style={{width:"100px"}} src="../daulogo.svg"/></h2>
                </Box>
            </Grid>
            <Grid item>
                <Typography variant="h4">Send Coins</Typography>
            </Grid>
            <Grid item>
                <Collapse in={sendSuccess}>
                    <Alert
                        action={
                        <IconButton
                            aria-label="close"
                            color="inherit"
                            size="small"
                            onClick={() => {
                            setSendSuccess(false)
                            }}
                        >
                            <Close fontSize="inherit" />
                        </IconButton>
                        }
                    >
                        Order created successfully!
                    </Alert>
                </Collapse>
                <Collapse in={sendError}>
                    <Alert
                        severity="error"
                        action={
                        <IconButton
                            aria-label="close"
                            color="inherit"
                            size="small"
                            onClick={() => {
                            setSendError(false)
                            }}
                        >
                            <Close fontSize="inherit" />
                        </IconButton>
                        }
                    >
                        Sorry, error sending coin
                    </Alert>
                </Collapse>
                <form onSubmit={handleSend}>
                    <Box m={3}>
                        <TextField
                        value={recieverAddress}
                        onChange={(event)=>{setRecieverAddress(event.target.value)}}
                        InputProps={{endAdornment: <IconButton> <QrCode/> </IconButton>}}
                        label="To Address"/>
                    </Box>
                    <Box m={3}>
                        <TextField
                        value={password}
                        onChange={(event)=>{setPassword(event.target.value)}}
                        label="Password"/>
                    </Box>

                    <Box m={3}>
                        <TextField
                        value={amount}
                        onChange={(event)=>{setAmount(event.target.value)}}
                        label="Amount"/>
                    </Box>
                    <Box>
                        <Button type="submit" variant="contained" color="primary">
                            {loading? "": "Send"}
                            {loading && <CircularProgress size={27} />}
                        </Button>
                    </Box>
                </form>
            </Grid>
        </Grid>
    )
}


export default SendCoin