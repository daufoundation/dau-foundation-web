import { Button, CircularProgress, Collapse, Grid, IconButton, TextField } from "@material-ui/core";
import { Box } from "@mui/system";
import React, { useState } from "react";
import ConfigData from '../config.json'
import axios from "axios";
import { Alert } from "@mui/material";
import { Close } from "@mui/icons-material";
import { useHistory } from "react-router";


function AddWallet(){
    const [address, setAddress] = useState()
    const [loading, setLoading] = useState(false)
    const [getWalletSuccess, setGetwalletSuccess] = useState()
    const [getWalletError, setGetWalletError] = useState()
    const history = useHistory()
    const API = ConfigData.API
    
    function handleAddClick(){
        setLoading(true)
        console.log(address)
        axios.post(API+"/wallet/info",{Address: address}, {
            withCredentials: false,
            headers: { 'Content-Type': 'application/json'
          }})
        .then((resp)=>{
            console.log(resp)
            setGetwalletSuccess(true)
            setLoading(false)
            // keep wallet 
            var wallets = JSON.parse(localStorage.getItem("wallets"))
            wallets.push(resp.data.data)
            console.log(wallets)
            localStorage.setItem("wallets", JSON.stringify(wallets))
            
            history.push("/")
        }, (error)=>{
            console.log(error.response.data)
            setGetWalletError(true)
            setLoading(false)
        })
    }
    return (
        <Grid container direction="column" justifyContent="center" >
            <Grid item>
                <Box textAlign="center" spacing={5} m={5} >
                    <h2  alignContent="center"><img style={{width:"100px"}} src="../daulogo.svg"/></h2>
                </Box>
            </Grid>
            <Grid item>
                <Collapse in={getWalletSuccess}>
                    <Alert
                        action={
                        <IconButton
                            aria-label="close"
                            color="inherit"
                            size="small"
                            onClick={() => {
                            setGetwalletSuccess(false)
                            }}
                        >
                            <Close fontSize="inherit" />
                        </IconButton>
                        }
                    >
                        Order created successfully!
                    </Alert>
                </Collapse>
                <Collapse in={getWalletError}>
                    <Alert
                        severity="error"
                        action={
                        <IconButton
                            aria-label="close"
                            color="inherit"
                            size="small"
                            onClick={() => {
                            setGetWalletError(false)
                            }}
                        >
                            <Close fontSize="inherit" />
                        </IconButton>
                        }
                    >
                        Sorry, error getting wallet
                    </Alert>
                </Collapse>
                <form>
                    <Box m={5}>
                        <TextField
                        onChange={(event)=>{setAddress(event.target.value)}}
                        label="Address"/>
                    </Box>
                    <Box m={5}>
                        <Button onClick={handleAddClick} variant="contained" color="primary">
                            {loading? "": "Add Wallet"}
                            {loading && <CircularProgress size={27} />}
                        </Button>
                    </Box>
                </form>
            </Grid>
        </Grid>
    )

}


export default AddWallet