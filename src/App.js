import logo from './logo.svg';
import './App.css';
import { createTheme } from '@material-ui/core';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import AllWallets from './components/AllWallets';
import { ThemeProvider } from '@material-ui/styles';
import { Create, Send, SingleBed } from '@mui/icons-material';
import CreateWallet from './components/CreateWallet';
import AddWallet from './components/AddWallet';
import SingleWallet from './components/SingleWallet';
import SendCoin from './components/Send';


function App() {
  
  const theme = createTheme({
    palette: {
        primary: {
          // light: will be calculated from palette.primary.main,
          main: '#4577C1',
          // dark: will be calculated from palette.primary.main,
          // contrastText: will be calculated to contrast with palette.primary.main
        },
        
        green:{
          main: "#00FF00"
        }
    },
});
  return (
    <Router>
    <ThemeProvider theme={theme}>
    <div className="App">
      <Switch>
        <Route exact path="/">
          <AllWallets/>
        </Route>
        <Route exact path="/create_wallet">
          <CreateWallet/>
        </Route>
        <Route exact path="/add_wallet">
          <AddWallet/>
        </Route>
        <Route   path="/wallet/:address">
          <SingleWallet/>
        </Route>
        <Route   path="/send/:address">
          <SendCoin/>
        </Route>
      </Switch>
    </div>
    </ThemeProvider>
    </Router>
  );
}

export default App;
